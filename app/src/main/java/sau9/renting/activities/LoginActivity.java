package sau9.renting.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import sau9.renting.R;
import sau9.renting.helper.GCMHelper;


public class LoginActivity extends AppCompatActivity {

    private CallbackManager callbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //if (Profile.getCurrentProfile() != null)
        //  startActivity(new Intent(LoginActivity.this, MainActivity.class));

        callbackManager = CallbackManager.Factory.create();

        final LoginButton loginButton = (LoginButton) findViewById(R.id.fb_btn);
        loginButton.setReadPermissions(Arrays.asList("email", "public_profile"));
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {

            @Override
            public void onSuccess(LoginResult loginResult) {

                getUserInfo(loginResult);
                //startActivity(new Intent(LoginActivity.this, MainActivity.class));
            }

            @Override
            public void onCancel() {
                Log.i("sau", "cancel");
            }

            @Override
            public void onError(FacebookException error) {
                Log.i("sau", error.toString());
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void getUserInfo(LoginResult loginResult) {

        GraphRequest request = GraphRequest.newMeRequest(
                loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject me, GraphResponse response) {
                        if (response.getError() != null) {
                            // handle error
                        } else {
                            try {

                                String email = response.getJSONObject().get("email").toString();
                                String fname = response.getJSONObject().get("first_name").toString();
                                String lname = response.getJSONObject().get("last_name").toString();

                                Map<String, String> map = new HashMap<>();
                                map.put("email", email);
                                map.put("fname", fname);
                                map.put("lname", lname);

                                GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(LoginActivity.this);
                                GCMHelper gcmHelper = new GCMHelper(LoginActivity.this, gcm);

                                if (gcmHelper.checkPlayServices(LoginActivity.this)) {

                                    String regid = gcmHelper.getRegistrationId(LoginActivity.this);
                                    if (regid.isEmpty()) {
                                        // gcmHelper.registerWithGCM(map);
                                        new GCMRegistration(getApplicationContext(), gcm, GCMHelper.getAppVersion(LoginActivity.this), map).execute();
                                    }
                                }

                                startActivity(new Intent(LoginActivity.this, MainActivity.class));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id, first_name, last_name, email,gender, birthday, location"); // Parámetros que pedimos a facebook
        request.setParameters(parameters);
        request.executeAsync();
    }
}
