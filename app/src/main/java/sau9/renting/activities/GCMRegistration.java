package sau9.renting.activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import sau9.renting.BackgroundApp;

/**
 * Created by SaurabhHome on 12/21/15.
 */

class GCMRegistration extends AsyncTask<Void, Void, String> {

    private static final String TAG = "GCMRelated";
    private final Context context;
    private final int appVersion;
    private final Map<String, String> map;
    private GoogleCloudMessaging gcm;

    public GCMRegistration(Context context, GoogleCloudMessaging gcm, int appVersion, Map<String, String> map) {
        this.context = context;
        this.gcm = gcm;
        this.appVersion = appVersion;
        this.map = map;
    }


    @Override
    protected String doInBackground(Void... arg0) {
        String msg, regid = null;
        try {
            if (gcm == null) {
                gcm = GoogleCloudMessaging.getInstance(context);
            }
            String SENDER_ID = "124311738990";
            regid = gcm.register(SENDER_ID);
            msg = "Device registered, registration ID=" + regid;
            storeRegistrationId(context, regid);
        } catch (IOException ex) {
            msg = "Error :" + ex.getMessage();

        }
        BackgroundApp.print(msg);
        return regid;
    }

    @Override
    protected void onPostExecute(String regid) {
        //Toast.makeText(context, "Registration Completed. Now you can see the notifications from Rent Out", Toast.LENGTH_SHORT).show();
        if (regid != null && regid != "")
            sendRegistrationIdToBackend(regid);
    }

    private void storeRegistrationId(Context ctx, String regid) {
        final SharedPreferences prefs = ctx.getSharedPreferences(BackgroundApp.GCM_REG, Context.MODE_PRIVATE);
        Log.i(TAG, "Saving regId on app version " + appVersion);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("registration_id", regid);
        editor.putInt("appVersion", appVersion);
        editor.apply();

    }


    private void sendRegistrationIdToBackend(final String regid) {
        String url = BackgroundApp.GCM_SERVLET;

        StringRequest sr = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Toast.makeText(BackgroundApp.getAppInstance(), "Registration Completed. Now you can see the notifications", Toast.LENGTH_SHORT).show();
                storeRegistrationId(context, regid);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                map.put("regid", regid);
                map.put("action", "reg");
                return map;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        BackgroundApp.getAppInstance().addToRequestQueue(sr);

    }


}
