package sau9.renting;

import android.app.Application;
import android.location.Address;
import android.location.Geocoder;
import android.text.TextUtils;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.facebook.FacebookSdk;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.LatLng;

import java.util.List;

/**
 * Created by SaurabhHome on 12/20/15.
 */
public class BackgroundApp extends Application {

    public static final String GCM_REG = "GCM";
    public static final String ITEM_SERVLET = "http://172.17.8.31:8080/RentalApp/Item";
    public static final String GCM_SERVLET = "http://172.17.8.31:8080/RentalApp/GCM";
    private static final String TAG = "Saurabh Jain";
    private static BackgroundApp appInstance;
    private ImageLoader imageLoader;
    private RequestQueue mRequestQueue;

    public static synchronized BackgroundApp getAppInstance() {
        return appInstance;
    }

    public void setAppInstance(BackgroundApp appInstance) {
        BackgroundApp.appInstance = appInstance;
    }

    public static void print(String string) {
        Log.i(TAG, string);
    }

    public static LatLng getLocationFromAddress(String strAddress) {

        Geocoder coder = new Geocoder(getAppInstance());
        List<Address> address;
        LatLng p1 = null;

        try {
            address = coder.getFromLocationName(strAddress, 5);
            if (address == null) {
                return null;
            }
            Address location = address.get(0);
            location.getLatitude();
            location.getLongitude();

            p1 = new LatLng(location.getLatitude(), location.getLongitude());

        } catch (Exception ex) {

            ex.printStackTrace();
        }

        return p1;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

    public ImageLoader getImageLoader() {
        getRequestQueue();
        if (imageLoader == null) {
            imageLoader = new ImageLoader(this.mRequestQueue,
                    new LruBitmapCache());
        }
        return this.imageLoader;

    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        setAppInstance(this);
        initializeGoogleMap();
        initialIzeFacebook();
    }

    private void initialIzeFacebook() {
        FacebookSdk.sdkInitialize(getAppInstance());
    }

    private void initializeGoogleMap() {
        MapsInitializer.initialize(this);
    }
}
