package sau9.renting.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by SaurabhHome on 12/20/15.
 */
public class Items {

    @SerializedName("itemId")
    private String itemID;
    @SerializedName("itemCategory")
    private String itemCategory;
    @SerializedName("itemName")
    private String itemName;
    @SerializedName("itemDescription")
    private String itemDescription;
    @SerializedName("itemOwner")
    private String itemOwner;
    @SerializedName("itemRenter")
    private String itemRenter;
    @SerializedName("itemUrl")
    private String itemUrl;
    @SerializedName("itemIsAvailable")
    private boolean isAvailable;
    @SerializedName("itemDuration")
    private double itemDuration;
    @SerializedName("itemPrice")
    private double itemPrice;
    @SerializedName("itemDeposit")
    private double itemDeposit;
    @SerializedName("lat")
    private double latitude;
    @SerializedName("lng")
    private double longitude;

    public String getItemID() {
        return itemID;
    }

    public void setItemID(String itemID) {
        this.itemID = itemID;
    }

    public String getItemCategory() {
        return itemCategory;
    }

    public void setItemCategory(String itemCategory) {
        this.itemCategory = itemCategory;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemDescription() {
        return itemDescription;
    }

    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    public String getItemOwner() {
        return itemOwner;
    }

    public void setItemOwner(String itemOwner) {
        this.itemOwner = itemOwner;
    }

    public String getItemRenter() {
        return itemRenter;
    }

    public void setItemRenter(String itemRenter) {
        this.itemRenter = itemRenter;
    }

    public String getItemUrl() {
        return itemUrl;
    }

    public void setItemUrl(String itemUrl) {
        this.itemUrl = itemUrl;
    }

    public boolean isAvailable() {
        return isAvailable;
    }

    public double getItemDuration() {
        return itemDuration;
    }

    public void setItemDuration(double itemDuration) {
        this.itemDuration = itemDuration;
    }

    public void setIsAvailable(boolean isAvailable) {
        this.isAvailable = isAvailable;
    }

    public double getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(double itemPrice) {
        this.itemPrice = itemPrice;
    }

    public double getItemDeposit() {
        return itemDeposit;
    }

    public void setItemDeposit(double itemDeposit) {
        this.itemDeposit = itemDeposit;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
