package sau9.renting.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import java.util.List;

import sau9.renting.BackgroundApp;
import sau9.renting.R;
import sau9.renting.models.Items;

/**
 * Created by SaurabhHome on 12/20/15.
 */
public class NearByAdapter extends RecyclerView.Adapter<NearByAdapter.NearByViewHolder> {


    private final Context context;
    private final List<Items> data;

    public NearByAdapter(Context context, List<Items> data) {

        this.context = context;
        this.data = data;
    }

    @Override
    public NearByViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.near_by_list_item, parent, false);


        return new NearByViewHolder(v);

    }

    @Override
    public void onBindViewHolder(NearByViewHolder holder, int position) {

        Items item = data.get(position);
        holder.bind(item);
    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    public void swap(List<Items> data) {
        this.data.clear();
        this.data.addAll(data);
        notifyDataSetChanged();
    }

    class NearByViewHolder extends RecyclerView.ViewHolder {

        TextView name;
        TextView category;
        TextView description;
        TextView price;
        TextView deposit;
        TextView availability;
        TextView status;
        Button rent;
        Button wishlist;
        NetworkImageView image;

        public NearByViewHolder(View view) {
            super(view);

            image = (NetworkImageView) view.findViewById(R.id.near_by_item_image);
            name = (TextView) view.findViewById(R.id.near_by_item_name);
            category = (TextView) view.findViewById(R.id.near_by_item_category);
            description = (TextView) view.findViewById(R.id.near_by_item_description);
            price = (TextView) view.findViewById(R.id.near_by_item_price);
            deposit = (TextView) view.findViewById(R.id.near_by_item_deposit);
            availability = (TextView) view.findViewById(R.id.near_by_item_availability);
            status = (TextView) view.findViewById(R.id.near_by_item_status);
            rent = (Button) view.findViewById(R.id.near_by_item_rent);
            wishlist = (Button) view.findViewById(R.id.near_by_item_wish);
        }

        public void bind(Items item) {
            //String url = "http://store.storeimages.cdn-apple.com/4884/as-images.apple.com/is/image/AppleInc/aos/published/images/i/ph/iphone5s/selection/iphone5s-selection-hero-2015?wid=300&hei=300&fmt=png-alpha&qlt=95&.v=1441814122616";
            name.setText(item.getItemName());
            description.setText(item.getItemDescription());
            price.setText("Rent: $ " + item.getItemPrice());
            deposit.setText("Deposit: $ " + item.getItemDeposit());
            status.setText("Duration: " + item.getItemDuration() + " H");
            category.setText("Cat: " + item.getItemCategory());
            String available = item.isAvailable() ? "Y" : "N";
            availability.setText("Availability: " + available);

            image.setDefaultImageResId(R.mipmap.loading);
            ImageLoader imageLoader = BackgroundApp.getAppInstance().getImageLoader();
            image.setDefaultImageResId(R.mipmap.loading);
            image.setImageUrl(item.getItemUrl(), imageLoader);
        }
    }
}
